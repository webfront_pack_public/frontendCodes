import i from "./utils/insertStyle.js";
// 检测横屏还是竖屏
export const isXY = (callback) => {
  window.addEventListener(
    "onorientationchange" in window ? "orientationchange" : "resize",
    function () {
      if (window.orientation == 180 || window.orientation == 0) {
        callback && callback({ label: "竖屏", n: 1 });
      }
      if (window.orientation == 90 || window.orientation == -90) {
        callback && callback({ label: "横屏", n: 2 });
      }
    },
    false
  );
};
// 改变横竖屏时的样式
export const changeXYstyle = (styleX, styleY) => {
  i(
    `
    @media all and (orientation : landscape) {
      ${styleX}
  }
  @media all and (orientation : portrait) {
    ${styleY}
  }
  `,
    "xy"
  );
};
