// content 是样式【必须】，id是唯一的标签【可选】
export default (content, id) => {
  if (id) {
    document.getElementById(id)?.remove();
  }

  let styleNode = document.createElement("style");
  styleNode.id = id;
  styleNode.setAttribute("type", "text/css");
  styleNode.innerHTML = content;
  let headNode = document.querySelector("head");
  headNode.appendChild(styleNode);
};
