export default function (dom) {
  if (HTMLElement && typeof HTMLElement === "function") {
    return dom instanceof HTMLElement;
  } else {
    return false;
  }
}
