// 元素是否出现在可视区，name是元素的id/class/tagName
export const domIsShow = (name, callback) => {
  if (window?.IntersectionObserver && name) {
    let items = [...document.querySelectorAll(name)];
    let io = new IntersectionObserver(
      (entries) => {
        entries.forEach((item) => {
          if (item.intersectionRatio) {
            callback && callback({ label: "show", n: 1 });
          }
        });
      },
      {
        root: null,
        rootMargin: "0px 0px",
        threshold: 1,
      }
    );
    items.forEach((item) => io.observe(item));
  }
};
