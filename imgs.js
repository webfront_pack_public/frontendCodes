// 图片预加载
export const imgPreloader = (imgs) => {
  if (imgs && Array.isArray(imgs) && imgs.length) {
    for (let i = 0, len = imgs.length; i < len; i++) {
      images[i] = new Image();
      images[i].src = imgs[i];
    }
  } else {
    throw "参数不合法";
  }
};

// 图片预览
export const imgPreview = (input, view, callback) => {
  input.addEventListener("change", (event) => {
    if (!event.target.files || event.target.files.length == 0) return false;
    var img = document.createElement("img");
    img.src = ((file) => {
      let url = null;
      if (window.createObjectURL != undefined) {
        // basic
        url = window.createObjectURL(file);
      } else if (window.URL != undefined) {
        // webkit or chrome
        url = window.URL.createObjectURL(file);
      } else if (window.URL != undefined) {
        // mozilla(firefox)
        url = window.URL.createObjectURL(file);
      }
      return url;
    })(event.target.files[0]);
    img.style.width = "100%";
    view.appendChild(img);
    callback && callback(img);
  });
};
