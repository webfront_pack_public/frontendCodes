// 唤起手机拨号
export const callNumber = (code, box, callBack) => {
  var a = document.createElement("a");
  // <a href='tel:12345678910'>phone</a>;
  a.href = "tel:" + code;
  a.innerText = code;
  box.appendChild(a);
  callBack && callBack(a);
};

// 安卓里发起发短信 - 允许给多个号码发短信
export const androidCallSms = (tels, msg, box, callBack) => {
  var a = document.createElement("a");
  if (typeof tels == string || typeof tels == "number") {
    a.href = "sms:" + tels + "?body=" + (msg || "");
  } else if (Array.isArray(tels) && tels.length) {
    a.href = "sms:" + tels.join() + "?body=" + (msg || "");
  }
  //  <a href="sms:12345678910,12345678911?body=hello">android message</a>
  box.appendChild(a);
  callBack && callBack(a);
};

// ios发短信
export const iosCallSms = (tels, msg, box, callBack) => {
  var a = document.createElement("a");
  if (typeof tels == string || typeof tels == "number") {
    a.href = "sms:/open?addresses=" + tels + "&body=" + (msg || "");
  } else if (Array.isArray(tels) && tels.length) {
    a.href = "sms:/open?addresses=" + tels.join() + "&body=" + (msg || "");
  }
  //  <a href="sms:/open?addresses=12345678910,12345678911&body=hello">ios message</a>
  box.appendChild(a);
  callBack && callBack(a);
};
