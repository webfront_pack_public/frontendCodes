/* 通过给不需要打印的元素添加 .no-print 类名可以在打印的时候过滤 */
import insertStyle from "./utils/insertStyle.js";
export const print = () => {
  insertStyle(
    `
  @media print {
    .no-print {
        display: none;
    }
}
  `,
    "print"
  );
  window.print();
};
