import isDom from "./utils/isDom.js";
// 元素全屏放大
export const elFullscreen = (dom) => {
  if (!isDom(dom)) {
    return new Error("非dom元素，不可以操作");
  }
  /* tips: 改事件必须在事件中才可以正常运行 */
  if (dom.requestFullscreen) {
    dom.requestFullscreen();
  } else if (dom.mozRequestFullScreen) {
    //不同浏览器之间需要添加前缀
    dom.mozRequestFullScreen();
  } else if (dom.msRequestFullscreen) {
    dom.msRequestFullscreen();
  } else if (dom.webkitRequestFullscreen) {
    dom.webkitRequestFullScreen();
  }
};
// 页面全屏放大
export const winFullscreen = () => {
  if (!document.fullscreenElement) {
    document.documentElement.requestFullscreen();
  } else {
    if (document.exitFullscreen) {
      document.exitFullscreen();
    }
  }
};
