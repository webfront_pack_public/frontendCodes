// 该方式可以劫持窗口关闭和刷新
export const holdUnload = () => {
  window.onbeforeunload = function (content) {
    return content || "你确定要离开吗？";
  };
};
