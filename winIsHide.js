// tab页是否隐藏和显示
export const winIsHide = (cb) => {
  const { hidden, visibilityChange } = (() => {
    let hidden, visibilityChange;
    if (typeof document.hidden !== "undefined") {
      // Opera 12.10 and Firefox 18 and later support
      hidden = "hidden";
      visibilityChange = "visibilitychange";
    } else if (typeof document.msHidden !== "undefined") {
      hidden = "msHidden";
      visibilityChange = "msvisibilitychange";
    } else if (typeof document.webkitHidden !== "undefined") {
      hidden = "webkitHidden";
      visibilityChange = "webkitvisibilitychange";
    }
    return {
      hidden,
      visibilityChange,
    };
  })();

  document.addEventListener(
    visibilityChange,
    () => {
      cb && cb(document[hidden]);
    },
    false
  );
};
