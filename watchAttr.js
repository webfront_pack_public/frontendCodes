// 监听dom元素属性修改
export const watchDomAttrChange = (el, cb, cancel) => {
  const observe = new MutationObserver((mutations) => {
    cb && cb(mutations);
  });
  observe.observe(el, {
    attributes: true,
  });

  cancel &&
    cancel(function () {
      return observe.disconnect();
    });
};
